# chat app
Welcome to the official readme.md file of the web-based chat app. Here you can chat with your friends,family,colleagues,your loved ones etc. You just have to enter the room and start chatting with everyone. You can enter the room using your gmail account. All your messages remain stored on the server if you need them in future for some purpose, well we all require screenshots at some time ;);)

To view this app in action, visit the link below, 
<https://chatapp-12e38.firebaseapp.com/home.html>
 or you can serve it from a local host.

This is a collective work of the hardworking members of the prestigious group called CodeRangers.
Developer-CodeRangers
Members-
1)Mr. Devansh Chaudhary (Frontend Development Head) 
2)Mr. Swapnil Narad (UI maintenance Head) 
3)Mr. Gagan Deep Singh (Backend Development Head)
