var db = firebase.firestore();
//loadMsg();
var callcount=0;
function addMessage(text)
{
    if(isUserSignedIn())
    {
    db.collection("msg").add({
        content:text,
        profilepic:getProfilePicUrl(),
        username:getUserName(),
        email:useremail,
        timestamp: firebase.firestore.FieldValue.serverTimestamp()
    })
    .then(function(docRef) {
        
    })
    .catch(function(error) {
        
    });
}
else{
    window.location.href="login.html";
}
}
function createMessageEle(msg)
{
    var div=document.createElement('div');
    var type="friend";
    if(msg.email==useremail){
        div.innerHTML='<div class="row"><div class="chat self"><div class="col s11"><span class="chat-message right">'+msg.content+'</span></div><div class="col s1 photoholder">'+
        '<div class="user-photo">'+
        '<img class="circle responsive-img" src="'+msg.profilepic+'"></div>'+
    '</div>';

    }
    else{
    div.innerHTML='<div class="row"><div class="chat '+type +'"><div class="col s1 photoholder">'+
            '<div class="user-photo">'+
            '<img class="circle responsive-img" src="'+msg.profilepic+'"></div>'+
        '</div>'+
        '<div class="col s11"><span class="chat-message left">'+'<span class="red-text">'+msg.username+"<br>"+"</span>"+msg.content+'</span></div>';
    }
    return div;

}

function shownewmsg(msg,type)
{
    //Update Ui accordingly
    console.log(msg.content);
    var holder=document.getElementById("holder");
    var msg=createMessageEle(msg);
    if(type==0)
    {
        holder.insertBefore(msg, holder.firstChild);
    }
    else
    {
        holder.appendChild(msg);
    }
    scroll();
   
}

function loadMsg() {
    console.log(isUserSignedIn());
    if(isUserSignedIn()){
    var query=db.collection("msg").orderBy('timestamp', 'desc');
    query.onSnapshot(function(snapshot) {
        
        snapshot.docChanges().forEach(function(change) {
            console.log("New change recieved-");
            console.log(change);
            if(change.type=="added")
          shownewmsg(change.doc.data(),callcount);
    });
    callcount=1;

    // Create the query to load the last 12 messages and listen for new ones.
});
}
else{
    window.location.href="login.html";
}
}

function send()
{
    var txt=document.getElementById("inp").value;
   document.getElementById("inp").value="";
    console.log(txt);
    if(txt.length!=0)addMessage(txt);
}

function scroll()
{
    var element=document.getElementById('holder');
    element.scrollTop = element.scrollHeight - element.clientHeight;
}