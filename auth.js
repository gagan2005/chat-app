
var hideOnLogOutElements = document.querySelectorAll('.hide-on-log-out');
var hideOnLogInElements = document.querySelectorAll('.hide-on-log-in');
var username = null;
var profilePicUrl = null;
var useremail=null;


function signOut() {
    // Sign out of Firebase.
    firebase.auth().signOut();
    window.location.href="home.html";
}

// Returns the signed-in user's profile pic URL.
function getProfilePicUrl() {
    return firebase.auth().currentUser.photoURL || '/images/profile_placeholder.png';
}

// Adds a size to Google Profile pics URLs.
function addSizeToGoogleProfilePic(url, size) {
    if (url.indexOf('googleusercontent.com') !== -1 && url.indexOf('?') === -1) {
        return url + '?sz=' + size;
    }
    return url;
}

// Returns the signed-in user's display name.
function getUserName() {
    return firebase.auth().currentUser.displayName;
}

// Returns true if a user is signed-in.
function isUserSignedIn() {
    return !!firebase.auth().currentUser;
}

function authStateObserver(user) {
    if (user) { // User is signed in!
        // Get the signed-in user's profile pic and name.
        profilePicUrl = getProfilePicUrl();
        username = getUserName();
        useremail=user.email;
        // Set the user's profile pic and name.
        loadMsg();
    }
}

// Initiate Firebase Auth.
function initFirebaseAuth() {
    // Listen to auth state changes.
    firebase.auth().onAuthStateChanged(authStateObserver);
}

initFirebaseAuth();